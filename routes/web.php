<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Customer
Route::get('palvision/customer/insert/{name}/{customerphone}/{customernumber}/{address}', 'CustomersController@store');

Route::get('palvision/customer/retrive/{id}', 'CustomersController@show');

Route::get('palvision/customer/update/{id}/{name}/{age}/{grade}', 'CustomersController@update');

Route::get('palvision/customer/delete/{id}', 'CustomersController@destroy');

// Product

Route::get('palvision/products/insert/{name}/{age}/{class}', 'ProductsController@store');

Route::get('palvision/products/retrive/{id}', 'ProductsController@show');

Route::get('palvision/products/update/{id}/{name}/{age}/{class}', 'ProductsController@update');

Route::get('palvision/products/delete/{id}', 'ProductsController@destroy');

// 
Route::get('palvision/sale/insert/{name}/{age}/{class}', 'SalesController@store');

Route::get('palvision/sale/retrive/{id}', 'SalesController@show');

Route::get('palvision/sale/update/{id}/{name}/{age}/{class}', 'SalesController@update');

Route::get('palvision/sale/delete/{id}', 'SalesController@destroy');

//

Route::get('palvision/shop/insert/{name}/{age}/{class}', 'ShopsController@store');

Route::get('palvision/shop/retrive/{id}', 'ShopsController@show');

Route::get('palvision/shop/update/{id}/{name}/{age}/{class}', 'ShopsController@update');

Route::get('palvision/shop/delete/{id}', 'ShopsController@destroy');

//

Route::get('palvision/vendor/insert/{name}/{age}/{class}', 'VendorsController@store');

Route::get('palvision/vendor/retrive/{id}', 'VendorsController@show');

Route::get('palvision/vendor/update/{id}/{name}/{age}/{class}', 'VendorsController@update');

Route::get('palvision/vendor/delete/{id}', 'VendorsController@destroy');