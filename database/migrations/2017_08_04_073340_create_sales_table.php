<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('Sales', function (Blueprint $table) {
            $table->string('id');
            $table->string('vender_id'); 
            $table->string('shops_id');
            $table->string('products');
            $table->string('date');
            $table->String('sumtotal');
            $table->unique('id');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('Sales');
    }
}
