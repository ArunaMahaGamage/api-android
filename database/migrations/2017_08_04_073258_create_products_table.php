<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('Products', function (Blueprint $table) {
            $table->string('id'); 
            $table->string('price');
            $table->string('type');
            $table->string('manufacturer');
            $table->timestamps();
            $table->unique('id');

        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('Products');
    }
}
